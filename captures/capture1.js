const Mongoose = require('../config/mongoose');
const MongoDB = require('../config/mongodb');
//Connect to mongodb
var connect_mongo = MongoDB();
var connect_mongoose = Mongoose();

var mongoose = require('mongoose');
const Product = mongoose.model('Product');

const variants = ['S', 'M', 'L', 'XL']
const totalRecords = 1200
const rounds = 10
const records = totalRecords / rounds

console.log('start of the writing database...')

for (let j = 0; j < rounds; j++) {
  let i = 0
  while (i < records) {
    i++;

    const order = createOrder()
    let variant = variants[Math.floor(Math.random() * variants.length)]

    let object = {
      productId: i,
      variant,
      order,
    }

    saveProduct(object);
  }

}

console.log('wrote all data to the database...')

function createOrder() {
  const date = randomDate(new Date(2005, 0, 1), new Date())
  const price = randomNumber()

  return {
    orderDate: date,
    price,
  }
}

function randomNumber() {
  const number = Math.floor(Math.random() * records + 1)

  return number
}

function randomDate(start, end) {
  return new Date(
    start.getTime() + Math.random() * (end.getTime() - start.getTime()),
  )
}

async function saveProduct(product) {
  let newProduct = new Product(product);
  await newProduct.save(async (err) => {
    if (err) {
        return res.json({success: false, errMessage: "Unknown errors occurred while saving new product."});
    } else {
    }
  }); 
}
