const Mongoose = require('../config/mongoose');
const MongoDB = require('../config/mongodb');
//Connect to mongodb
var connect_mongo = MongoDB();
var connect_mongoose = Mongoose();

var mongoose = require('mongoose');
const Product = mongoose.model('Product');
const cron = require('node-cron');

//Cron job for the past 30 days
cron.schedule('0 0 */30 * * *', async function() {
  let products = await Product.aggregate([ {
      $match: {"order.orderDate": {'$gte': new Date(Date.now() - 30 * 24 * 60 * 60 * 1000), '$lte': Date.now}}
    },
    {$group : {_id: "$variant", count: {$sum : 1}}}]
  );
  console.log("Accumulation of product variant orders in the past 30 days", products);
});

//Cron job for the past 90 days
cron.schedule('0 0 01 */3 * *', async function() {
  let products = await Product.aggregate([ {
      $match: {"order.orderDate": {'$gte': new Date(Date.now() - 90 * 24 * 60 * 60 * 1000), '$lte': Date.now}}
    },
    {$group : {_id: "$variant", count: {$sum : 1}}}]
  );
  console.log("Accumulation of product variant orders in the past 90 days", products);
});

//Cron job for the past 180 days
cron.schedule('0 0 01 */6 * *', async function() {
  let products = await Product.aggregate([ {
      $match: {"order.orderDate": {'$gte': new Date(Date.now() - 180 * 24 * 60 * 60 * 1000), '$lte': Date.now}}
    },
    {$group : {_id: "$variant", count: {$sum : 1}}}]
  );
  console.log("Accumulation of product variant orders in the past 180 days", products);
});


//Cron job for the past 365 days
cron.schedule('0 0 01 */12 * *', async function() {
  let products = await Product.aggregate([ {
      $match: {"order.orderDate": {'$gte': new Date(Date.now() - 365 * 24 * 60 * 60 * 1000), '$lte': Date.now}}
    },
    {$group : {_id: "$variant", count: {$sum : 1}}}]
  );
  console.log("Accumulation of product variant orders in the past 365 days", products);
});