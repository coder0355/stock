const express = require("express");
const Mongoose = require('../config/mongoose');
const MongoDB = require('../config/mongodb');
//Connect to mongodb
var connect_mongo = MongoDB();
var connect_mongoose = Mongoose();

var mongoose = require('mongoose');
const app = express();

require('../Product/Product.routes')(app);

const port = process.env.PORT || 5000;  //process.env.port is Heroku's port if you choose to deplay the app there
app.listen(port, () => console.log("Server up and running on port " + port));