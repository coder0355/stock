var mongoose = require('mongoose');

var ProductSchema = new mongoose.Schema({
    productId: {type: Number, default: 0},
    variant: {type: String, default: "XL"},
    order: {
        orderDate: {type:Date},
        price: {type: Number, default: 0}
    }
});

var Product = mongoose.model('Product', ProductSchema);

module.exports = Product;
