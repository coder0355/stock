var mongoose = require('mongoose');
const Product = mongoose.model('Product');

const fs = require('fs')
let writeStream = fs.createWriteStream('capture3.jsonl')

exports.calcFunc = async (req, res) =>  {
    let daysList = [30, 90, 180, 365];
    let output = {};
    let result = {};

    //Cron job for the past xx days
    for( let xx = 0 ; xx < daysList.length ; xx ++) {
        let days = daysList[xx];

        let products = await Product.aggregate([ {
            $match: {"order.orderDate": {'$gte': new Date(Date.now() - days * 24 * 60 * 60 * 1000), '$lte': Date.now}}
            },
            {$group : {_id: "$variant", cashFlow: {$sum: "$order.price"}, sales: {$sum : 1}}}]
        );
        let item = {};

        for(let i = 0 ; i < products.length ; i ++) {
            let variant = products[i]._id;
            let obj = {
                "productId": "???",
                "cashFlow": products[i].cashFlow,
                "sales": products[i].sales,
            }

            item[variant] = obj;
        }

        result[`${days}days`] = item;
    }

    output["bestSellers"] = result;
    writeStream.write(`${JSON.stringify(output)}`)

    writeStream.on('finish', () => {
        console.log('wrote all data to file')
    })
    
    writeStream.end()

    return res.json({bestSellers: result});

}
